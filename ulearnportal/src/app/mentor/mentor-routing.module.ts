import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
import { MentorStudentMessageComponent } from './mentor-student-message/mentor-student-message.component';
import { CourseUploadComponent } from './course-upload/course-upload.component';

const routes: Routes = [
  {path: '', component: MentorDashboardComponent },
  {path: 'dashboard', component: MentorDashboardComponent },
  {path: 'course', component: CourseUploadComponent},
 // {path: 'course/:id/details', component: CourseDetailsComponent},
  {path: 'mail', component: MentorStudentMessageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MentorRoutingModule { }
