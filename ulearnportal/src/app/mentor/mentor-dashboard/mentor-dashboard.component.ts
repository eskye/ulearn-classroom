import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared';
import { IDashboardService, DashboardService } from 'src/app/shared/services/dashboard.service';
 

@Component({
  selector: 'app-mentor-dashboard',
  templateUrl: './mentor-dashboard.component.html',
  styleUrls: ['./mentor-dashboard.component.css']
})
export class MentorDashboardComponent extends BaseComponent  implements OnInit {

  constructor(private dashboardService: DashboardService) {
    super(null, null, dashboardService);
  }

  ngOnInit() {
    this.dashboardService.getMentorDashboard().subscribe(res => {
      console.log(res);
    }, error => console.log(error));
  }

}
