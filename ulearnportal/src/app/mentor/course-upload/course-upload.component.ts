import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { BaseComponent, ICourse, SystemConstant } from 'src/app/shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IPath, ILevel } from 'src/app/shared/common/model/IPath';
import { ScriptLoaderService } from 'src/app/shared/services/script-loader.service';
import { CourseService } from 'src/app/admin/services/course.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-course-upload',
  templateUrl: './course-upload.component.html',
  styleUrls: ['./course-upload.component.css']
})

export class CourseUploadComponent extends BaseComponent implements OnInit, AfterViewInit {

  courseform: FormGroup;
  paths: IPath[];
  levels: ILevel[];
  courses: ICourse[];
  playlist: [object];
  errorText: string;
  url: string = SystemConstant.IMAGE;
  @ViewChild('courseposter') courseposter: ElementRef;
    preview: any;
    yurl: any = '';

    constructor(private fb: FormBuilder, private scriptLoader: ScriptLoaderService,
       private courseService: CourseService, private sanitizer: DomSanitizer) {
      super();
    }

    ngOnInit() {
      this.listCourse();
      this.validate();
    }

    ngAfterViewInit(): void {
      this.scriptLoader.loadScripts('app-course-upload', [
        'assets/js/plugins/forms/styling/uniform.min.js',
        'assets/js/demo_pages/form_inputs.js',
        'assets/js/demo_pages/learning.js'
      ]);
    }

    validate() {
      this.courseform = this.fb.group({
         coursename: ['', Validators.required],
         description: ['', Validators.required],
         path: ['', Validators.required],
         level: ['', Validators.required],
         resource: ['', Validators.required],
        coursethumbnail: null
      });
    }

    onFileChange(event) {
      if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];
          this.courseform.get('coursethumbnail').setValue(file);
      }
    }
  loadFormData() {
    this.listPath();
    this.listLevel();
  }

  closeCloneModal() {
    const originalModal = $('#course_preview').clone();
  $(document).on('#course_preview', 'hidden.bs.modal', function () {
      $('#course_preview').remove();
      const myClone = originalModal.clone();
      $('body').append(myClone);
  });
  }
  closeModal() {
    $('.modal').on('hidden.bs.modal', function() {
      $('.modal-body').html('');
  });
  }
    clearFile() {
      this.courseform.get('coursethumbnail').setValue(null);
      this.courseposter.nativeElement.value = '';
    }

    private prepareSave(): FormData {
      const inputs = new FormData();
      inputs.append('coursename', this.courseform.get('coursename').value);
      inputs.append('description', this.courseform.get('description').value);
      inputs.append('path', this.courseform.get('path').value);
      inputs.append('level', this.courseform.get('level').value);
      inputs.append('banner', this.courseform.get('coursethumbnail').value);
      inputs.append('resource', this.courseform.get('resource').value);
      return inputs;
    }

    createCourse() {
      this.waiting = true;
      const formModel = this.prepareSave();
      this.courseService.createCourse(formModel).subscribe(res => {
        this.clearFile();
        this.waiting = true;
         this.reloadComponent();
        // $('#modalcourse').close();
        this.courseService.successAlert('Course created successfully');
      }, error => {
        this.clearFile();
        this.errorText = error;
        this.courseService.errorAlert(error);
      });
    }

    listCourse() {
      const pathid = 0;
      this.courseService.getCourse(pathid).subscribe(res => {
        this.courses = res;
      }, error => {
        this.courseService.errorAlert('An error occurred');
      });
    }

    listPath() {
      this.courseService.adminService.listPath().subscribe(res => {
        this.paths = res;
      }, error => {
       this.courseService.errorAlert('An error occurred');
      });
    }

     listLevel() {
      this.courseService.adminService.listLevel().subscribe(res => {
        this.levels = res;
      }, error => {
        this.courseService.errorAlert('An error occurred');
      });
    }

    getPlaylist(playlistId) {
      this.courseService.youtubeService.getplaylist(playlistId).subscribe(res => {
        this.playlist = (<any>res).items;
          // this.preview = (<any>this.playlist.items.slice(0, 1));
       this.preview = this.playlist.slice(0, 1);
       const videoId = this.preview[0].snippet.resourceId.videoId;
       this.yurl =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoId);

      }, error => {
        this.courseService.errorAlert('An error occurred');
      });
    }

}
