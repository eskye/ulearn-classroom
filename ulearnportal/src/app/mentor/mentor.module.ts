import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MentorRoutingModule } from './mentor-routing.module';
import { MentorDashboardComponent } from './mentor-dashboard/mentor-dashboard.component';
import { MentorStudentMessageComponent } from './mentor-student-message/mentor-student-message.component';
import { CourseUploadComponent } from './course-upload/course-upload.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    SharedModule,
    MentorRoutingModule
  ],
  declarations: [
    MentorDashboardComponent,
     MentorStudentMessageComponent,
     CourseUploadComponent
    ]
})
export class MentorModule { }
