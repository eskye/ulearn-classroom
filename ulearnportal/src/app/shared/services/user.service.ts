import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'https://jsonplaceholder.typicode.com';
  constructor(private htttp: HttpClient) { }

  getUserByEmail(email: string) {
    return this.htttp.get<any[]>(`${this.url}?email=${email}`);
  }
  fileValidator(file: FormData) {
    return this.htttp.post<any>(`${this.url}`, file);
  }
}
