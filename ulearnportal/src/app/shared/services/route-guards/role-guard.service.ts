import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole;
    const role = this.auth.role;
    if (!this.auth.isAuthenticated() || role !== expectedRole) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }

}

