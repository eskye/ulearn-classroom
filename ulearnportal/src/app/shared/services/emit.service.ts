import { Injectable } from '@angular/core';
import { IProfile } from '..';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class EmitService {

  data: any;
  isactive: boolean;
  isOpen: boolean;
  barcode: boolean;
  statelocals: any;
  pageHeader: any;
  breadcrumbName: any;
  // Observable variable

  private messageSource = new BehaviorSubject<boolean>(this.isactive);
  currentMessage$ = this.messageSource.asObservable();


  private sidebarOpenSource = new BehaviorSubject<boolean>(this.isOpen);
  currentSideBarState$ = this.sidebarOpenSource.asObservable();

  private dataBarcodeSource = new BehaviorSubject<boolean>(this.barcode);
  currentCheck$ = this.dataBarcodeSource.asObservable();

  private dataSource = new BehaviorSubject<IProfile>(this.data);
  currentData$ = this.dataSource.asObservable();

  private stateSource = new BehaviorSubject<any>(this.statelocals);
  stateData$ = this.stateSource.asObservable();

  // Page Header Message Hub
  private headerHub = new BehaviorSubject<any>(this.pageHeader);
  headerHubData$ = this.headerHub.asObservable();

  private breadcrumb = new BehaviorSubject<any>(this.breadcrumbName);
  breadcrumbData$ = this.breadcrumb.asObservable();

  constructor() { }

  changeMessage(message: boolean) {
    this.messageSource.next(message);
  }
  getPageHeaderMessage(title: any) {
    this.headerHub.next(title);
  }

  getBreadCrumbMessage(breadcrumb: any) {
    this.breadcrumb.next(breadcrumb);
  }

  changeSideBarState(message: boolean) {
    this.sidebarOpenSource.next(message);
  }

  checkBarcodeStatus(message: boolean) {
    this.dataBarcodeSource.next(message);
  }

  changeData(data: any) {
    this.dataSource.next(data);
  }

  changeLocalState(state: any) {
    this.stateSource.next(state);
  }
}
