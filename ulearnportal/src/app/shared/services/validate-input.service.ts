import { Injectable } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidateInputService {


  static passwordMatchValidator(g: FormGroup): any {
    return g.get('password').value === g.get('confirmpassword').value
      ? null : {'mismatch': true};
  }

  static passwordValidator(control?: AbstractControl) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    // (?!.*\s)          - Spaces are not allowed
    if (control.value.match(/^(?=.*\d)(?=.*[a-zA-Z!@#$%^&*])(?!.*\s).{6,100}$/)) {
      return null;
    } else {
      return { 'invalidPassword': true };
    }
  }


}
