import { Injectable, Injector } from '@angular/core';
import { IDashboard } from '../common/model/IDashboard';
import { IRootService } from '../common/interface';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { routes } from '../constant';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/Error/ErrorHandler';

export interface IDashboardService extends IRootService<IDashboard> {
  getAdminDashboard(): Observable<IDashboard>;
  getMentorDashboard(): Observable<IDashboard>;
}

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends RootService<IDashboard> implements IDashboardService  {
  

constructor(httpClient: HttpClient, inject: Injector, auth: AuthService) {
  super(httpClient, inject, auth);
}

getAdminDashboard(): Observable<IDashboard> {
 return this.getlist(routes.DASHBOARD.ADMIN).pipe(map(res => {
   return res;
 }), catchError(ErrorHandler.handleError));
}

getMentorDashboard(): Observable<IDashboard> {
  return this.get(this.auth.Udata.id, routes.DASHBOARD.MENTOR).pipe(map(res =>{
    return res;
  }), catchError(ErrorHandler.handleError));
}


}
