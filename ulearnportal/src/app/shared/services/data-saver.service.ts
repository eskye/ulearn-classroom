import { Injectable, Injector } from '@angular/core';
import * as crypto from 'crypto-js';
import { SystemConstant } from '../index';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class DataSaverService {
  private dataHolder: any = {};
  constructor(private cookieService: CookieService, private inject : Injector) { }

  private encryptData(unEncryptedData) {
    return crypto.AES.encrypt(unEncryptedData, SystemConstant.RSA).toString();
  }

  private decryptData(encryptedData) {
    return crypto.AES.decrypt(encryptedData.toString(), SystemConstant.RSA).toString(crypto.enc.Utf8);
  }

  public persistData(key, encryptedNumber) {
    const data = (key === '_sstt23tken5_' ? encryptedNumber : this.encryptData(encryptedNumber));
    // sessionStorage.setItem(key, data);
    this.cookieService.set(key, data);
  }

  public removePersitedData(key: string): void {
    // window.sessionStorage.removeItem(key);
    this.cookieService.delete(key, '/');
  }

  public getAllCookie(){
    return this.cookieService.getAll();
  }
  get cookService(): CookieService {
    return this.inject.get(CookieService);
  }

  public removeAllPersistedData(): void {
    // window.sessionStorage.clear();
    this.cookieService.deleteAll('/');
  }

  public getPersistedData(key): any {
    if (this.cookieService.get(key) ) {
      return key === '_sstt23tken5_' ? this.cookieService.get(key) : this.decryptData(this.cookieService.get(key));
    } else {
      return false;
    }
    // if (sessionStorage.getItem(key) ) {
    //   return key === '_sstt23tken5_' ? sessionStorage.getItem(key) : this.decryptData(sessionStorage.getItem(key));
    // } else {
    //   return false;
    // }
  }

  private removePersistedData(key) {
    return this.cookieService.delete(key);
    // return sessionStorage.removeItem(key);
  }

  keepData(key, sharedData): void {
    if (key === 'role') {
      return this.persistData('__jtoh67823_', sharedData);
    }

    if (key === 'token') {
      return this.persistData('_sstt23tken5_', sharedData);
    }

    if (key === 'key') {
      return this.persistData('_sj45jmker23h_', sharedData);
    }
    if (key === 'active') {
      return this.persistData('_sghjkaj5678lknl__', sharedData);
    }

    if (key === 'udata') {
      return this.persistData('_sjhdmer45dsr_', JSON.stringify(sharedData));
    }

    if (key === 'form_tracker') {
      return this.persistData('_form_tracker__', sharedData);
    }

    if (key === 'UA-ID') {
      return this.persistData('__Uaaajej34332__', sharedData);
    }

    if (key === 'location') {
      return this.persistData('__loailis2344__', sharedData);
    }

    this.dataHolder[key] = sharedData;
  }

  public getData(key) {

    if (key === 'role') {
      return this.getPersistedData('__jtoh67823_') ?
        this.getPersistedData('__jtoh67823_') : false;
    }

    if (key === 'token') {
      return this.getPersistedData('_sstt23tken5_');
    }

    if (key === 'active') {
      return this.getPersistedData('_sghjkaj5678lknl__');
    }

    if (key === 'key') {
      return this.getPersistedData('_sj45jmker23h_');
    }
    if (key === 'udata') {
      return JSON.parse(this.getPersistedData('_sjhdmer45dsr_'));
    }
    if (key === 'other') {
      return this.getPersistedData('_$ssidek/ddneds__');
    }

    if (key === 'form_tracker') {
      return this.getPersistedData('_form_tracker__');
    }
    if (key === 'UA-ID') {
      return this.getPersistedData('__Uaaajej34332__');
    }

    if (key === 'location') {
      return this.getPersistedData('__loailis2344__');
    }
    // if (key === 'key') {
    //   return this.getPersistedData('_sj45jmker23h_') ?
    //     JSON.parse(this.getPersistedData('_sjkrejker23sd_')) : false;
    // }
    //
    // if (key === 'bvnTracker') {
    //   return this.getPersistedData('_sjkskkl509sd_');
    // }

    return key ? this.dataHolder[key] : this.dataHolder;
  }

  removeData(key?) {
    if (key) {
      if (key === 'phoneNo') {
        this.removePersistedData('__ss67823');
      }
      delete this.dataHolder[key];
    } else {
      this.dataHolder = {};
    }
  }

  checkValue(event, dataLength?) {
    const { value } = event.target;
    const key = event.keyCode > 0 ? event.keyCode : event.charCode;

    if (key === 8 || key === 44 || key === 46 || key >= 37 && key <= 40 || key >= 48 && key <= 57) {
      // Prevent characters %, &, (, and ' on Chrome, Firefox & Opera browsers
      if (key >= 37 && key <= 40 && (event.key === '%' || event.key === '&' || event.key === '(' || event.key === '\'')) {
        return false;
      }
      // Prevent characters %, &, (, and ' on Safari browser
      if (key >= 37 && key <= 40 && event.keyIdentifier === '') {
        return false;
      }
      // *** Firefox Bug fix *** This allows the user to still be able to use arrow keys and backspace when the maxlength is reached
      if (key === 8 || key >= 37 && key <= 40) {
        return true;
      }
      return dataLength ? !(value.length > (dataLength - 1)) : true;
    }
    return false;
  }

}
