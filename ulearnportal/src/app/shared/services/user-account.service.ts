import { Injectable } from '@angular/core';
import { IRootService, IProfile, ISignup } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/Error/ErrorHandler';

export interface IUserAccountService extends IRootService<IProfile> {
  updateProfile(profilehmo: FormData): Observable<IProfile>;
  createAccount(data: ISignup): Observable<ISignup>;
}
@Injectable({
  providedIn: 'root'
})
export class UserAccountService extends RootService<IProfile> implements IUserAccountService {
 
  constructor(httpClient: HttpClient) {
    super(httpClient);
  }


  updateProfile(profilehmo: FormData, ): Observable<IProfile> {
    return this.httpClient.post<IProfile>('routes.UPDATEPROFILE', profilehmo)
      .pipe(map(profile => {
        return profile;
      }));
  }

  createAccount(data: ISignup): Observable<ISignup> {
   return this.httpClient.post<ISignup>(routes.REGISTER, data).pipe(map(res => {
     return res;
   }), catchError(ErrorHandler.handleError));
  }



}
