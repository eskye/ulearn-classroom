import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { Playlist } from '../common/model/Playlist';
import { IRootService } from '../common/interface/IRootService';
import {
  HTTP_INTERCEPTORS, HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/Error/ErrorHandler';
import { SystemConstant } from '../constant';
export interface IYoutubeService extends IRootService<Playlist> {
  getplaylist(playlistId): Observable<any>;
  playlistList_page(playlistId, pageToken): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})



export class YoutubeService extends RootService<Playlist>  {
  private API_Youtube = 'https://www.googleapis.com/youtube/v3/playlistItems?';

  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient);
  }

  url(path: string) {
    return this.API_Youtube + path;
  }

  getplaylist(playlistId): Observable<any> {
    return this.getEx(this.url('part=snippet&maxResults=20&playlistId=' + playlistId + '&key=' + SystemConstant.YKEY ))
    .pipe(map(playlist => {
     return playlist as any;
    }), catchError(ErrorHandler.handleError));
}

playlistList_page(playlistId, pageToken): Observable<any> {

  // tslint:disable-next-line:max-line-length
  return this.getlist(this.url('part=snippet&maxResults=20&pageToken=' + pageToken + '&playlistId=' + playlistId + '&key=' + SystemConstant.YKEY))
    .pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));

// return this.http.get
// ("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&pageToken="+pageToken+"&playlistId="+playlistId+"&key="+this.key)
}
}
