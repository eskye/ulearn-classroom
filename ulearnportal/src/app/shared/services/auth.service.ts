import { Injectable, Injector } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DataSaverService } from './data-saver.service';
import { ILoggedInUser, routes } from '..';
import { Observable } from 'rxjs/internal/Observable';
import { RootService } from '../common/interface/RootService';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { IProfile } from '../common';




@Injectable({
  providedIn: 'root'
})
export class AuthService  {
  helper: JwtHelperService = new JwtHelperService();
  constructor(private httpClient: HttpClient, private injector: Injector, private store: DataSaverService) {

  }


  public isAuthenticated(): boolean {
    const token = this.Token;
    const check = this.helper.isTokenExpired(token);
    if (check) {
      this.isExpired();
    }
    return !check;
  }

  public isExpired() {
        this.refreshToken();
  }
  get role() {
    return this.store.getData('role');

  }

  get decodeToken() {
    return this.helper.decodeToken(this.Token);
  }

  get Token() {
    return this.store.getData('token');
  }

  get active() {
    return this.store.getData('active');
  }


  get Key() {
    return this.store.getData('key');
  }

  get Udata() {
    return this.store.getData('udata');
  }

  loggedInUser(): Observable<any> {
    if (this.isAuthenticated()) {
      const reqHeader = { 'Content-Type': 'application/json' };
      return this.httpClient.post('routes.LOGGEDIN', { userId: this.Key }).pipe(map(loggedUser => {
        return loggedUser;
      }));
    }
  }

   getUserDetail(): Promise<any> {
    return new Promise((resolve, reject) => {
      const reqHeader = {'Content-Type': 'application/json'};
      this.httpClient.post('', {userId: this.Key})
        .subscribe(data => resolve(data), err => reject(err));
    });
  }



  refreshToken() {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.Token}` }
      );
      this.httpClient.patch(routes.REFRESH, null, {headers: reqHeader}).subscribe(res => {
        this.store.keepData('token', (<any>res).data.token);
      });
  }
  passwordChanger(password) {
    // tslint:disable-next-line:no-shadowed-variable
    return this.httpClient.post('', password).pipe(map(password => {
      return password;
    }));
  }

}
