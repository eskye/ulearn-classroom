import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { IProfile } from '../common';


import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs/internal/operators/catchError';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
    providedIn: 'root'
})
export class LoggedInResolver implements Resolve<IProfile> {
    constructor(private auth: AuthService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.auth.loggedInUser().pipe(map(res => {
            return {...res};
        }));
    }
}


