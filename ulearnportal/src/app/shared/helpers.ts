// import * as $ from 'jquery';
declare let $: any;

export class Helpers {
  static loadStyles(tag, src) {
    if (Array.isArray(src)) {
      $.each(src, function (k, s) {
        $(tag).append($('<link/>').attr('href', s).attr('rel', 'stylesheet').attr('type', 'text/css'));
      });
    } else {
      $(tag).append($('<link/>').attr('href', src).attr('rel', 'stylesheet').attr('type', 'text/css'));
    }
  }

  static unwrapTag(element) {
    $(element).removeAttr('appunwraptag').unwrap();
  }

  /**
   * Set title markup
   * @param title
   */
  static setTitle(title) {
    $('.m-subheader__title').text(title);
  }

  /**
   * Breadcrumbs markup
   * @param breadcrumbs
   */

  static bodyClass(strClass) {
    $('body').attr('class', strClass);
  }
}
