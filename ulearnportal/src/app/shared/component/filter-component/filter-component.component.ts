import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-filter-component',
  templateUrl: './filter-component.component.html',
  styleUrls: ['./filter-component.component.css']
})
export class FilterComponentComponent implements OnInit {
@Input() filter = false;
@Input() isUserType = true;
  constructor() { }

  ngOnInit() {
  }

}
