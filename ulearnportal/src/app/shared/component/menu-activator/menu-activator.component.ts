import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { EmitService } from '../../services/emit.service';



@Component({
  selector: 'app-menu-activator',
  templateUrl: './menu-activator.component.html',
  styleUrls: ['./menu-activator.component.css']
})
export class MenuActivatorComponent implements OnInit {
  @Input() divswitcher = false;
  @Input() link: any;
  @Input() message: any;
  @Output() proceed = new EventEmitter<any>();
  constructor(private router: Router, private emitserve: EmitService) { }

  ngOnInit() {
  }

  setUp() {
    this.proceed.emit(this.link);
    this.router.navigate([this.link]);
  }

}
