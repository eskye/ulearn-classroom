import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {IProfile} from '../../common/model/IProfile';
import {SystemConstant} from '../../constant';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {IPassword} from '../../common/model/IAccount';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileSanitizer} from '../../../utils/file-sanitizer';
import { BaseComponent } from '../../common/base/BaseComponent';

@Component({
  selector: 'app-iprofile',
  templateUrl: './iprofile.component.html',
  styleUrls: ['./iprofile.component.css']
})
export class IprofileComponent extends BaseComponent implements OnInit {
@Input() profile: IProfile;
  url: string = SystemConstant.IMAGE;
  password: IPassword;
  edit = false;
  message = '';
  haccountSetting: FormGroup;
  @Output()Profiledata = new EventEmitter<FormData>();
  @ViewChild('uploader') uploader: ElementRef ;
  fileExtensionError = false;
  fileExtensionMessage: string;
  public profilePic: any;
  constructor(private fb: FormBuilder, private apiService: AuthService, private toastrService: ToastrService) {
    super();
  }

  ngOnInit() {
  }

  // ValidateField() {
  //   this.haccountSetting = this.fb.group({
  //     profilePic: null,
  //     firstName: [this.profile.firstName, Validators.required],
  //     lastName: [this.profile.lastName, Validators.required],
  //     gender: [this.profile.gender, Validators.required],
  //     email: [{value: this.profile.email, disabled: true}],
  //     Address: [this.profile.address, Validators.required],
  //     phoneNumber: [this.profile.phoneNumber, Validators.required],
  //   });
  // }
  openProfileToggle() {
    this.edit = !this.edit;
  }


  onFileChange(event) {
    const file = event.target.files[0];

    if (FileSanitizer.fileValidator(file, true)) {

      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.profilePic = reader.result;
        };
      }
      if (event.target.files.length > 0) {
        this.haccountSetting.get('profilePic').setValue(file);
      }
    } else {
      this.haccountSetting.setErrors({ 'invalid': true });
      this.fileExtensionMessage = 'Only photos allowed!!';
      this.fileExtensionError = false;
      this.toastrService.warning(this.fileExtensionMessage);
    }

  }
  private prepareUser(): FormData {
    const formData = new FormData();
    formData.append('firstName', this.haccountSetting.get('firstName').value);
    formData.append('lastName', this.haccountSetting.get('lastName').value);
    formData.append('phoneNumber', this.haccountSetting.get('phoneNumber').value);
    formData.append('address', this.haccountSetting.get('Address').value);
    formData.append('gender', this.haccountSetting.get('gender').value);
    formData.append('Picture', this.haccountSetting.get('profilePic').value);
    formData.append('Id', this.apiService.Key);
    return formData;
  }

  hprofileUpdate() {
   // Filters.disableButton(this.haccountSetting);
    const formModel = this.prepareUser();
    this.Profiledata.emit(formModel);
    this.clearFile();
  }

  clearFile() {
    this.haccountSetting.get('profilePic').setValue(null);
    this.uploader.nativeElement.value = '';
  }
  changePasswordEvent(passworddata) {
    // alert(JSON.stringify(passworddata));
    this.password = passworddata;
    this.password.id = this.apiService.Key;
    this.apiService.passwordChanger(this.password).subscribe(() => {
      this.toastrService.success('Password have been changed successfully');
    }, error => {
      this.toastrService.error(error.error.message);
    });
  }

}
