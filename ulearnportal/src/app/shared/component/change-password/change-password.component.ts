import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IPassword} from '../../common/model/IAccount';
import {ValidateInputService} from '../../services/validate-input.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  data: IPassword;
  @Output()Pchange = new EventEmitter<any>();
  changePasswordForm: FormGroup;
  constructor() { }
  message = '';
  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      currentpassword: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, ValidateInputService.passwordValidator]),
      confirmpassword: new FormControl('', Validators.required),
    }, ValidateInputService.passwordMatchValidator.bind(this));
  }

  changePassword() {
    this.data = this.changePasswordForm.value;
    this.Pchange.emit(this.data);
    this.changePasswordForm.reset();
  }

}
