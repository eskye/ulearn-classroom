import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../common/base/BaseComponent';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent extends BaseComponent implements OnInit {

  constructor(router: Router) {
    super(null, router);
  }

  ngOnInit() {
  }

}
