import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/internal/observable';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(public auth: AuthService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({ headers: req.headers.set('Authorization', `Bearer ${this.auth.Token}`) });

        return next.handle(authReq);
    }

}

// export const InterceptorSkipHeader = 'X-Skip-Interceptor';
// @Injectable()
// export class SkippableInterceptor implements HttpInterceptor {
//
//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     if (req.headers.has(InterceptorSkipHeader)) {
//       const headers = req.headers.delete(InterceptorSkipHeader);
//       return next.handle(req.clone({ headers }));
//     }
//   }
//
// }
