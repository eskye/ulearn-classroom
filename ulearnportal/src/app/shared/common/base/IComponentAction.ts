import { IConfirmSettings } from '../confirm-settings';
import { IQueryOptions, IFilter } from 'src/app/shared/common/interface/IQueryOptions';


export interface IComponentAction {
    editorOptions: any;
    settings: IConfirmSettings | any;
    url: any;
    query: IQueryOptions;
    items: any;
    states:any;
    pager: any;
    size: number;
    hideFilter: boolean;
    filter: any;
    filterarray: IFilter[];
    paginationConfig?: { count: number; page: number; total: number };
    pageChanged: (event) => void;
    changePage: () => void;
    init: () => void;
    goBack();
    goTo(url);
    go(route, id);
    goToNav(route);
    setupPagination();
    getCountry();
    getPageInfoDescription(): string;
    toggleActive();
    toggleFilter();
    reloadComponent();

}
