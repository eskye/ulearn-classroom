import { IListCtrlScope } from './IListCtrlScope';
import { IRootObject } from '../interface/IRootObject';
import { RootService } from '../interface/RootService';
import { OnInit } from '@angular/core';
import { Location } from '@angular/common';

export abstract class BaseListComponent implements OnInit {
    createLink: string;
    editLink: string;

    constructor(public Resource: RootService<IRootObject>, public $scope?: IListCtrlScope, public config?) {
    }

    ngOnInit(): void {
        this.$scope.waiting = true;
        this.$scope.orderByExpression = { column: 1, direction: 1 };
        this.$scope.filter = {};
        this.$scope.toggleActive = (item) => this.toggleActive(item, this.$scope.url);
        this.$scope.goBack = () => this.goBack();
        this.$scope.backLink = {};
    }

    toggleActive(item, url) {
        if (confirm('Are sure you ?')) {
            this.$scope.waiting = true;
            this.Resource.toggleActive(item, url).subscribe((data) => {
                    this.$scope.waiting = false;
                    this.$scope.setupPagination();
                    alert('Action successful!');
                }, (response) => {
                    alert('Error occured while performing the action');
                    this.$scope.waiting = false;
                });
        }
    }

    resetFiltering() {
        this.$scope.filter = {};
        this.setupPagination();
    }

    init() {
        this.initScope();
        this.setupPagination();
        this.bindScopeMethods();
    }

    initScope() {
        this.$scope.filterIsClosed = true;
        this.$scope.page = 1;
        this.$scope.paginationConfig = {
            count: 10,
            page: 1,
            total: 0
        };
        this.$scope.waiting = false;
        this.$scope.pager = { page: 1 };
        this.$scope.setupPagination = () => this.setupPagination();
        this.$scope.getTotalPages = () => this.getTotalPages();
        this.$scope.resetFiltering = () => this.resetFiltering();
        this.$scope.getPageInfoDescription = () => this.getPageInfoDescription();
        this.$scope.getCheckedItems = () => this.getCheckedItems();
        this.$scope.checkAll = () => this.checkAll();
        this.$scope.orderExpressionChanged = () => this.orderExpressionChanged();
        this.$scope.selectedIds = [];
    }

    getCheckedItems() {
        let checked = 0;
        this.$scope.selectedIds = [];
        if (this.$scope.items) {
            for (let i = 0; i < this.$scope.items.length; i++) {
                if (this.$scope.items[i].selected) {
                    checked++;
                    this.$scope.selectedIds.push(this.$scope.items[i].id);
                }
            }
        }
        return checked;
    }

    checkAll() {
        this.$scope.items.forEach((item) => {
            item.selected = !this.$scope.selectedAll;
        });
    }

    toggleSelectItem(index?) {
        this.$scope.items[index].selected = !this.$scope.items[index].selected;
    }

    getIndexSeed($index: number) {
        return (this.$scope.pager.page - 1) * this.$scope.paginationConfig.count + $index + 1;
    }

    getPageInfoDescription() {
        if (this.$scope.items) {
            return 'Showing ' + (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1) + 1) + ' to ' +
                (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1)
                    + this.$scope.items.length) + ' of ' + this.$scope.paginationConfig.total;
        }
        return '';
    }

    getTotalPages() {
        return Math.ceil(this.$scope.paginationConfig.total / this.$scope.paginationConfig.count);
    }

    bindScopeMethods() {
        this.$scope.pageChanged = () => this.pageChanged();
        this.$scope.deleteObject = (item) => this.deleteObject(item, this.$scope.url);
        this.$scope.reloadPage = () => this.reloadPage();
        this.$scope.getIndexSeed = (index) => this.getIndexSeed(index);
    }

    setupPagination() {
        this.$scope.waiting = true;
        this.Resource.count(Object.assign({
            count: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page ,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify(this.$scope.filter)
        }, this.$scope.$stateParams, this.config, this.$scope.filter),  this.$scope.url)
        .subscribe(data => {
            this.$scope.waiting = false;
            this.$scope.paginationConfig.total = data.total;
            this.$scope.items = data.items;
            this.$scope.hideFilter = false;
        // tslint:disable-next-line:no-shadowed-variable
        }, error => {
            this.$scope.waiting = false;
            console.log(error);
        });
    }

    pageChanged() {
        this.$scope.waiting = true;
        this.Resource.query(Object.assign({
            count: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page ,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify(this.$scope.filter)
        }, this.$scope.$stateParams, this.config, this.$scope.filter),  this.$scope.url)
            .subscribe((data) => {
            this.$scope.items = data.items;
            this.$scope.pager.page = this.$scope.paginationConfig.page;
            this.$scope.waiting = false;
        }, (data) => {
            this.$scope.waiting = false;
        });
    }

    deleteObject(item: IRootObject, url) {
        if (confirm('Are sure you want to delete the item?')) {
            this.$scope.waiting = true;
            this.Resource.delete(item, url)
            .subscribe(() => {
                this.$scope.waiting = false;
                this.setupPagination();
            }, (response) => {
                alert(response.data.message);
                this.$scope.waiting = false;
            });
        }
    }

    reloadPage() {
        this.ngOnInit();
    }

    orderExpressionChanged() {
        if (this.$scope.order.column && this.$scope.order.direction) {
            this.$scope.orderByExpression =  this.$scope.order.column + ' ' + this.$scope.order.direction;
            this.pageChanged();
        } else {
            this.$scope.orderByExpression = null;
        }
    }

    goBack(location?: Location) {
              location.back();
        // this.$scope.$state.go(this.$scope.backLink.forwardLink || this.$scope.backLink.state, Object.assign({},
        //     this.$scope.$stateParams, this.config, this.$scope.item));
    }
}
