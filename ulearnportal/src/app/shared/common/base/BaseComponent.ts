import { IComponentAction } from './IComponentAction';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { IConfirmSettings } from '../confirm-settings';
import { IRootService, IQueryOptions } from '../interface';
import { IFilter } from '../interface/IQueryOptions';
import { OnInit } from '@angular/core';
export abstract class BaseComponent implements IComponentAction, OnInit {

  constructor(
    public location?: Location,
    public router?: Router,
    public resource?: IRootService<any>
  ) {}
  editorOptions = {
    charCounterCount: true,
    // tslint:disable-next-line:max-line-length
    toolbarButtons: [
      'fontFamily',
      '|',
      'fontSize',
      '|',
      'paragraphFormat',
      '|',
      'bold',
      'italic',
      'underline',
      'underline',
      'strikeThrough',
      'outdent',
      'indent',
      'clearFormatting',
      'undo',
      'redo',
      'codeView',
      '|',
      'quote',
      'insertHR'
    ],
    fontFamilySelection: true,
    fontSizeSelection: true,
    paragraphFormatSelection: true,
    heightMin: 100,
    heightMax: 500
  };
  settings: IConfirmSettings | any = {
    overlay: true,
    overlayClickToClose: true,
    showCloseButton: true,
    confirmText: 'Yes',
    declineText: 'No'
  };
  query: IQueryOptions;
  filterarray: IFilter[];
  waiting: boolean;
  url: any;
  items: any;
  states: any;
  hideFilter = false;
  size = 10;
  filter: any = {};
  changePage: () => void;
  paginationConfig = {
    count: this.size || 50,
    page: 1,
    total: 0
};
pager: any = { page: 1, size: 50 };
  init() {
   this.setupPagination();
   this.filterarray = [
     {key: 'Active', value: 1},
     {key: 'Inactive', value: 0}
   ];
    }
  goBack() {
    this.location.back();
  }
  go(route, id) {
    this.router.navigate([route, id]);
  }
  goToNav(route) {
    this.router.navigate([route]);
  }
  // tslint:disable-next-line:no-shadowed-variable
  goTo(url: string) {
    this.router.navigateByUrl(url);
  }
  ngOnInit() {}
  reloadComponent() {
     this.ngOnInit();
  }
  setupPagination() {
    this.waiting = true;
    if (this.filter.isActive === 'Active') {
      this.filter = {
        isActive: 1
      };
    } else {
      this.filter = {
        isActive: 0
      };
    }
   return this.resource
      .query(
        Object.assign({
          count: this.paginationConfig.count,
          page: this.paginationConfig.page,
          orderByExpression: this.query.orderByExpression,
          whereCondition: JSON.stringify(this.filter)
        }),
        this.url
      ).subscribe(res => {
        this.waiting = false;
        this.paginationConfig.total = res.data.total;
        this.items = res.data.items;
        this.hideFilter = false;
    // tslint:disable-next-line:no-shadowed-variable
    }, error => {
        this.waiting = false;
        console.log(error);
    });
}

  getPageInfoDescription(): string {
    if (this.items) {
        return 'Showing ' + (this.paginationConfig.count * (this.pager.page - 1) + 1) + ' to ' +
            (this.paginationConfig.count * (this.pager.page - 1)
                + this.items.length) + ' of ' + this.paginationConfig.total;
    }
    return '';
}

pageChanged(event) {
  this.paginationConfig.page = event;
    this.waiting = true;
    return this.resource.query(Object.assign({
        count: this.paginationConfig.count,
        page: this.paginationConfig.page,
        orderByExpression: this.query.orderByExpression,
        whereCondition: JSON.stringify(this.filter)
    }),  this.url)
        .subscribe((data) => {
        this.items = data.data.items;
        this.pager.page = this.paginationConfig.page;
        this.waiting = false;
    }, (data) => {
        this.waiting = false;
    });
}
toggleActive() {
  throw new Error('Method not implemented.');
}

toggleFilter() {
  this.hideFilter = !this.hideFilter;
}
getCountry() {
  const apiUrl = '../../../assets/countries-data.json';
  return this.resource.getlist(apiUrl).subscribe((res)=>{
    this.states = res;
  });
}


}
