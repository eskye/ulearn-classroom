import { IOrganization } from './IOrganization';
import { IProvider } from './IProvider';
import { IHmo } from './IHmo';
import { IRootObject } from '../interface/IRootObject';



/**
 * Created by Sunkee on 26-Aug-18.
 */
export class IEnrollee extends IRootObject {

  surname: string;
  lastName: string;
  gender: string;
  address: string;
  nationality: string;
  state: string;
  lga: string;
  key: string;
  maritalstatus: string;
  age: number;
  imageurl: string;
  phoneno: string;
  provider: IProvider;
  organization: IOrganization;
  uhmo: IHmo;



}


export interface IEnrolleeSetting extends IEnrollee {
  employeeid: string;
  nationality: string;
  state: string;
  lga: string;
  dateofbirth: string;
}
export interface IEnrolleeUploader {
  enrolleeFile: string|any;
  orgId: string;
  hmoId: any;
  orgEmail: string;
}

export interface IEmployeeData extends IEnrollee {
  isActivated: boolean;
}

export interface IChangeRequestProvider extends IRootObject{
   reason: string;
   enrolleeId: string;
}
