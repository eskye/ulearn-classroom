import { IRootObject } from '../interface/IRootObject';
export interface IAccount extends IRootObject {
  email: string;
  password?: string;
  confirmpassword?: string;
  Role: string;
  scheme?: number;
  Id?: string;
  isSubAccount: boolean;
}

export interface IPassword extends IAccount {
  currentpassword: string;

}

export interface ITokenparams extends IAccount {
  token: string;
  token_type: string;
  expires_in: string;
  role: any;
  user: any;

}

export interface ISignup{
  email: string;
  name: string;
  password: string;
  role: string;
  phone:string;
}

export interface IUserLogin extends IRootObject {
  email: string;
  password: string;
}

export interface IResetPassword extends IRootObject, IAccount {
  code: string;
}
