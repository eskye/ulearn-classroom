import { IRootObject } from '../interface/IRootObject';
export interface IProvider extends IRootObject {
    providersId: number;
    providerName: string;
    providerEmail: string;
    providerAddress: string;
    providerPhone: string;
    providerLogo: string|any;
    providerLocation: string;
    latitude: number;
    longitude: number;
    providerDatecreated: string;
  }
export class IRootInvestigation {
  enrollee: string;
  provider: string;

}
  export interface IComplaint extends IRootInvestigation {
    complaint: string;
  }

  export interface IMedication extends IRootInvestigation {
    medication: string;
  }