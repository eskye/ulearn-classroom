import { IRootObject } from '../interface/IRootObject';

export interface Playlist extends IRootObject {
 kind?: string;
 etag?: string;
  items?: [object];
 pageInfo?: string;


}
