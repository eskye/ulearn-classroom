import { IRootObject } from '../interface/IRootObject';

export interface IOrganization extends IRootObject{
    orgName: string;
    orgEmail: string;
    orgPhone: string;
    orgAddress: string;
    orgUrl: string|any;
    orgLocation: string;
  }