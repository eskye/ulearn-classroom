import { IRootObject } from '../interface/IRootObject';

export interface ICourse extends IRootObject {
    coursename: string;
    banner: boolean;
    path_id: string;
    pathname?: string;
    level_id: string;
   course_banner: string;
   coursedescription: string;
   course_resource: string;
  coursecode: string;
}
