import { IRootObject } from '../interface';

export interface IDashboard extends IRootObject {
    courses?: number ;
    mentors?: number;
    paths?: number;
    students?: number;

}
