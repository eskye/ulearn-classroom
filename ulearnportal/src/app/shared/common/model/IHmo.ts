import { IOrganization } from './IOrganization';
import { IEnrollee } from './IEnrollee';
import { IRootObject } from '../interface/IRootObject';

export interface IHmo extends IRootObject{
    hmoId?: number|any;
    hmod: number;
    hmoName: string;
    hmoAddress: string;
    hmoLocation: string;
    hmoEmail: string;
    hmoPhone: string;
    hmoLogoUrl: string|any;
    hmoKey: string;
    Datecreated: string;
    Organization: IOrganization[];
    Enrollee: IEnrollee[];
  }