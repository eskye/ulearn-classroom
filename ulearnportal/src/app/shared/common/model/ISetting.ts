import { IRootObject } from '../interface/IRootObject';

export interface ISetting extends IRootObject{
    userId: string;
    charge: number;
    action: string;
}

export interface IFacility extends IRootObject {
    userId: string;
    planId?: number;
    providerId?: string;
    packageCode?: string;
  }
