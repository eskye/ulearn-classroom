import { IRootObject } from '../interface/IRootObject';

export interface IPath extends IRootObject {
pathname: string;
description: string;
pathcode: string;
}

export interface ILevel extends IRootObject {
    levelname: string;
    levelcode: string;
}
