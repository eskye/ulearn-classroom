import {IRootObject} from '../interface/IRootObject';
export interface ICounter extends IRootObject {
organization: number;
 enrollees: number;
 provider: number;
 activeEmployees: number;
  unactiveEmployees: number;
 subscription: string;
}
