import { IRootObject } from '../interface/IRootObject';
import { IData } from '../interface';



/**
 * Created by Sunkee on 26-Aug-18.
 */
export interface IOthersetting extends IRootObject {
  employeeid: string;
  nationality: string;
  state: string;
  lga: string;
  maritalstatus: string;
  surname?: string;
  email: string;
  dateofbirth: string;
}
export interface IProfile extends IRootObject {

  address: string;
  name: string;
  phone: string;
  gender: string;
  email: string;
  profilePic: string|any;
}

export interface ILoggedInUser extends IRootObject {
  IsActive: boolean;
  Role: string;
  Email: string;
}
