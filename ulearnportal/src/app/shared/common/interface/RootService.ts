import { Observable } from 'rxjs/internal/Observable';
import { IRootObject } from './IRootObject';
import { IRootService } from './IRootService';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { IQueryOptions } from './IQueryOptions';
import { ICountModel } from './ICountModel';
import { QueryBuilder } from './QueryBuilder';
import { Injector } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService, ConfirmSettings } from '@jaspero/ng-confirmations';
import {AuthService} from '../../services/auth.service';


export class RootService<T extends IRootObject> implements IRootService<T> {

    constructor(public httpClient: HttpClient, private inject?: Injector, public auth?: AuthService) {
    }

    count(queryOptions: IQueryOptions, url: string): Observable<ICountModel<T>> {
        return this.httpClient
            .get<ICountModel<T>>(`${url}/${QueryBuilder.toQueryString(queryOptions)}`)
            .pipe(map(data => {
                return data as ICountModel<T>;
            }));
    }


    query(queryOptions: IQueryOptions, url: string): Observable<ICountModel<T>> {

      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` }
        );
      return this.httpClient
            .get<ICountModel<T>>(`${url}/${QueryBuilder.toQueryString(queryOptions)}`, {headers: reqHeader})
            .pipe(map(data => {
                return data as ICountModel<T>;
            }));
    }

    create(item: T, url: string): Observable<T> {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient
            .post<T>(`${url}`, JSON.stringify(item), {headers: reqHeader});
    }

    post(url: string, body: any, headers?: any): Observable<T> {
      const reqHeader = new HttpHeaders( headers ||
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient.post<T>(`${url}`, body, { headers: reqHeader });
    }
    update(item: T, url: string): Observable<T> {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient
            .put<T>(`${url}/${item.id}`, JSON.stringify(item), {headers: reqHeader})
            .pipe(map(data => {
                return data as T;
            }));
    }

    delete(item: T, url: string) {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient
            .delete(`${url}/${item.id}`, {headers: reqHeader});
    }

    get(id: any, url: string): Observable<T> {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient
            .get(`${url}/${id}`, {headers: reqHeader})
            .pipe(map(data => {
                return data as T;
            }));
    }


  getEx(url: string): Observable<any> {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json'});

    return this.httpClient
      .get(`${url}`, {headers: reqHeader})
      .pipe(map(data => {
        return data as any;
      }));
  }

    getlist(url: string, param?: any, header?: any): Observable<T> {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
      return this.httpClient.get(`${url}`, {headers: reqHeader}).pipe(map(data => {
            return data as T;
        }));
    }

    getquery(url: string, queryKey: string, param?: any): Observable<any> {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
        const paramval = new HttpParams().set(queryKey, param);
        return this.httpClient.get(`${url}`, { params: paramval, headers: reqHeader}).pipe(map(data => {
            return data;
        }));
    }


    details(id: number, url: string) {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
        return this.httpClient
            .get(`${url}/${id}`, {headers: reqHeader})
            .pipe(map(data => {
                return data as T;
            }));
    }

    toggleActive(item: T, url: string) {
      const reqHeader = new HttpHeaders(
        { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': `Bearer ${this.auth.Token}` });
        return this.httpClient
            .put<T>(`${url}/${item.id}`, JSON.stringify(item), {headers: reqHeader})
            .pipe(map(data => {
                return data as T;
            }));
    }

    getService(TService): any {
       return this.inject.get(TService);
    }
     private get alert(): ToastrService {
         return this.inject.get(ToastrService);
     }

     private get _confirmation(): ConfirmationService {
        return this.getService(ConfirmationService);
     }

     confirm(title: string, message: string, options: ConfirmSettings): Observable<any> {
       return this._confirmation.create(title, message, options);
    }
    successAlert(msg: any, title?: any) {
     this.alert.success(msg, title);
    }
    errorAlert(msg: any, title?: any) {
        this.alert.error(msg, title);
    }
    warningAlert(msg: any, title?: any): any {
       this.alert.warning(msg, title);
      }



}
