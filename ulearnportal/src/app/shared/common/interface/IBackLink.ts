export interface IBackLink {
    state?: string;
    config?: {};
    forwardLink?: string;
}
