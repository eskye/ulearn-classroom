import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import { throwError } from 'rxjs';
export class ErrorHandler {
    // static handleError (error: any): any[] {
    //     let errors = [];
    //     let modelState: {};
    //     if (error.responseJSON) {
    //         errors.push(error.responseJSON.error_description);
    //     } else if (error.data ) {
    //         if (error.data.modelState) {
    //             modelState = error.data.modelState;
    //             // tslint:disable-next-line:forin
    //             for (const key in modelState) {
    //                 errors = errors.concat(modelState[key]);
    //             }
    //         } else if (error.data.message) {
    //             errors.push(error.data.message);
    //         }
    //     }
    //     return errors;
    // }


  static handleError(error: HttpErrorResponse) {
    // console.error('server error:', error.error);
    const errors: any = error.error;
    if (errors) {
      let modelStateError = '';
      for (const key in errors) {
        if (errors[key]) {
          modelStateError += errors[key] + '\n';
        }
      }
      if (modelStateError) {
        return throwError(modelStateError || 'Unexpected error occurred');
      }
    }

    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
      // Use the following instead if using lite-server
      // return Observable.throw(err.text() || 'backend server error');
    }
    return throwError(error || 'Server error');
  }

  static ErrorConnection(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  static ErrorServerConnection(error: HttpErrorResponse) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
      // Use the following instead if using lite-server
      // return Observable.throw(err.text() || 'backend server error');
    }
    return throwError(error || 'Server error');
  }

  private Errorhandler(error: any) {
    // const applicationError = http.get('Application-Error');
    const applicationError = '';
    if (applicationError) {
      return throwError(applicationError);
    }
    const serverError = error.json();
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }

    return throwError(modelStateErrors || 'server Error' );
  }
}
