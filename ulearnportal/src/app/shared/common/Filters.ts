import { FormGroup } from '@angular/forms';


/**
 * Created by Sunkee on 10-May-18.
 */
export class Filters {
  constructor() { }
  static isNumberKeyOnly(event): boolean {
    const charcode = (event.which) ? event.which : event.keyCode;
    return !(charcode > 31 && (charcode < 48 || charcode > 57));
  }

  static reset(addOrgForm: FormGroup): void {
    addOrgForm.reset();
  }

  static disableButton(addOrgForm: FormGroup): boolean {
    return addOrgForm.status !== 'INVALID';

  }
}
