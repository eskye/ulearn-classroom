import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
export function ValidateEmail(controlToValidate: string): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null => {
    if (c.value === null || c.value.length === 0) {
      return null;
    }

    const controltoValidate = c.root.get(controlToValidate);
    if (controltoValidate) {
      // tslint:disable-next-line:max-line-length
      if (c.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        return null;
      } else {
        return { 'validateemail': true };
      }
    }
  };
}
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[validateemail]'
})
export class EmailValidatorDirective implements Validator {
  // tslint:disable-next-line:no-input-rename
  @Input('validateemail') controlNameToCompare: string;
  constructor() { }

  validate(c: AbstractControl): ValidationErrors | null {
    return ValidateEmail(this.controlNameToCompare)(c);
  }

}
