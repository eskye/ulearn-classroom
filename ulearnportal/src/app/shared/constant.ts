import { environment } from '../../environments/environment.prod';
const BASEURL = environment.BASE_URL;
const BASE = environment.BASE;
export const routes = {
  REGISTER: BASEURL + '/account/users',
  LOGIN: BASEURL + '/account/login',
  REFRESH: BASEURL + '/account/refresh',
  GETUSERS: BASEURL + '/account/getusers',
  ACTIVATEUSER: BASEURL + '/account/activate',
  ACTIVATEUSERS: BASEURL + '/account/activate/all',
  ROLES: BASEURL + '/account/roles',
  CREATEPATH: BASEURL + '/path/create',
  LISTPATH: BASEURL + '/path/listpath',
  CREATELEVEL: BASEURL + '/level/create',
  LISTLEVEL: BASEURL + '/level/listlevel',
  CREATECOURSE: BASEURL + '/course/create',
  LISTCOURSE: BASEURL + '/course/list',
  GETUSERINCOURSE: BASEURL + '/course/getStudentCourse',
  ASSIGNMENTOR: BASEURL + '/course/assignMentor',
  GETENROLLMENT: BASEURL + '/course/getEnrolment',
  LATESTCOURSE: BASEURL + '/course/new',
  COURSEDETAILS: BASEURL + '/course/getcoursedetail',
  ENROL: BASEURL + '/course/enrol',
  GETMENTORS: BASEURL + '/account/mentors/list',
  DASHBOARD: {
    ADMIN: BASEURL + '/dashboard/admin',
    MENTOR: BASEURL + '/dashboard/mentor'
  }


};

export const SystemConstant = {
  ENCRYPTIONKEY: '</@$!!ulearn&!!)/>',
  RSA: `MIICXQIBAAKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQ
  WMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jE0NQ+Z+zEdhUTooNR
  aY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4gwQco1KRMDSmXSMkDwIDAQAB
  AoGAfY9LpnuWK5Bs50UVep5c93SJdUi82u7yMx4iHFMc/Z2hfenfYEzu+57fI4fv
  xTQ//5DbzRR/XKb8ulNv6+CHyPF31xk7YOBfkGI8qjLoq06V+FyBfDSwL8KbLyeH
  m7KUZnLNQbk8yGLzB3iYKkRHlmUanQGaNMIJziWOkN+N9dECQQD0ONYRNZeuM8zd
  8XJTSdcIX4a3gy3GGCJxOzv16XHxD03GW6UNLmfPwenKu+cdrQeaqEixrCejXdAF
  z/7+BSMpAkEA8EaSOeP5Xr3ZrbiKzi6TGMwHMvC7HdJxaBJbVRfApFrE0/mPwmP5
  rN7QwjrMY+0+AbXcm8mRQyQ1+IGEembsdwJBAN6az8Rv7QnD/YBvi52POIlRSSIM
  V7SwWvSK4WSMnGb1ZBbhgdg57DXaspcwHsFV7hByQ5BvMtIduHcT14ECfcECQATe
  aTgjFnqE/lQ22Rk0eGaYO80cc643BXVGafNfd9fcvwBMnk0iGX0XRsOozVt5Azil
  psLBYuApa66NcVHJpCECQQDTjI2AQhFc1yRnCU/YgDnSpJVm1nASoRUnU8Jfm3Oz
  uku7JUXcVpt08DFSceCEX9unCuMcT72rAQlLpdZir876MIGfMA0GCSqGSIb3DQEBA
  QUAA4GNADCBiQKBgQDlOJu6TyygqxfWT7eLtGDwajtN
  FOb9I5XRb6khyfD1Yt3YiCgQWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76
  xFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4
  gwQco1KRMDSmXSMkDwIDAQAB`,
  YKEY: 'AIzaSyBAt07IOnHdbDr9iOFz5OOZsA6C5ektEz4',
  YKEY3: 'AIzaSyB019ymG52xlaG1YsRIT7cO6japiXs9nr0',
  SESSIONKEY: '_hhaomssrhy234_',
  IMAGE: BASE + '/upload/',
  LEVEL_ACCESS: {
    's': 'Student',
    'm': 'Mentor',
    'a': 'Admin'
  },
  PREFIX: 'hmo'

};
