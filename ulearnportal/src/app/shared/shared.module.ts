import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompareValidatorDirective } from './directives/compare-validator.directive';
import { UniqueEmailValidatorDirective } from './directives/unique-email-validator.directive';
import { FileUploadValidatorDirective } from './directives/file-upload-validator.directive';
import { LoaderComponent } from './component/loader/loader.component';
import { BlockUIModule } from 'ng-block-ui';

import { EmailValidatorDirective } from './directives/email-validator.directive';
import { PasswordValidatorDirective } from './directives/password-validator.directive';

import { MenuActivatorComponent } from './component/menu-activator/menu-activator.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { IprofileComponent } from './component/iprofile/iprofile.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import {ValidateInputService} from './services/validate-input.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FooterComponent } from './component/footer/footer.component';
import { FilterComponentComponent } from './component/filter-component/filter-component.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
import { JasperoConfirmationsModule } from '@jaspero/ng-confirmations';
import { PaginationComponent } from './pagination/pagination.component';
import { SuccessComponent } from './component/success/success.component';
import { HrefPreventDefaultDirective } from './directives/href-prevent-default.directive';
import { AlertComponentComponent } from './component/alert-component/alert-component.component';
import {UnwrapTagDirective} from './directives/unwrap-tag.directive';
import { SafeResourcePipe } from './pipes/safe-resource.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot({
      message: 'Please Wait...',
    }),
    JasperoConfirmationsModule.forRoot(),

    NgbModule
  ],
  declarations: [
    CompareValidatorDirective,
    UniqueEmailValidatorDirective,
    EmailValidatorDirective,
    PasswordValidatorDirective,
    FileUploadValidatorDirective,
    LoaderComponent,
    MenuActivatorComponent,
    SafeHtmlPipe,
    IprofileComponent,
    ChangePasswordComponent,
    FooterComponent,
    FilterComponentComponent,
    SearchFilterPipe,
    PaginationComponent,
    SuccessComponent,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
    AlertComponentComponent,
    SafeResourcePipe
  ],
  exports: [
    CommonModule,
    CompareValidatorDirective,
    EmailValidatorDirective,
    PasswordValidatorDirective,
    UniqueEmailValidatorDirective,
    FileUploadValidatorDirective,
    HrefPreventDefaultDirective,
    LoaderComponent,
    MenuActivatorComponent,
    IprofileComponent,
    ChangePasswordComponent,
    SafeHtmlPipe,
    FooterComponent,
    FilterComponentComponent,
    SearchFilterPipe,
    SuccessComponent,
    AlertComponentComponent,
    SafeResourcePipe
  ],
  providers: [
    ValidateInputService
  ]
})
export class SharedModule { }
