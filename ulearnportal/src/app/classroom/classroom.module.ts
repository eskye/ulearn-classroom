import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassroomRoutingModule } from './classroom-routing.module';
import { ClassroomDashboardComponent } from './classroom-dashboard/classroom-dashboard.component';
import { ClassroomCourseComponent } from './classroom-course/classroom-course.component';
import { CourseDetailsComponent } from './classroom-course/course-details/course-details.component';
import {SharedModule} from '../shared/shared.module';
import { ClassTutorComponent } from './class-tutor/class-tutor.component';
import { BlockUIModule } from 'ng-block-ui';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BlockUIModule.forRoot({
      message: 'Please Wait...',
    }),
    ClassroomRoutingModule
  ],
  declarations: [
      ClassroomDashboardComponent,
      ClassroomCourseComponent,
      CourseDetailsComponent,
       ClassTutorComponent
      ]
})
export class ClassroomModule { }
