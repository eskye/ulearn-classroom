import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClassroomDashboardComponent} from './classroom-dashboard/classroom-dashboard.component';
import {ClassroomCourseComponent} from './classroom-course/classroom-course.component';
import {CourseDetailsComponent} from './classroom-course/course-details/course-details.component';
import { ClassTutorComponent } from './class-tutor/class-tutor.component';

const routes: Routes = [
  {path: '', component: ClassroomDashboardComponent },
  {path: 'dashboard', component: ClassroomDashboardComponent },
  {path: 'courses', component: ClassroomCourseComponent },
  {path: 'course/:id/details', component: CourseDetailsComponent},
  {path: 'course/lesson/:playlistId/:id/tutorroom', component: ClassTutorComponent},
  {path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassroomRoutingModule { }
