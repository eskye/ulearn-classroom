import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from 'src/app/admin/services/course.service';
import { ICourse } from 'src/app/shared/common/model/ICourse';
import { SystemConstant as constants } from 'src/app/shared/constant';
import { DomSanitizer } from '@angular/platform-browser';
import { NgBlockUI, BlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-class-tutor',
  templateUrl: './class-tutor.component.html',
  styleUrls: ['./class-tutor.component.css']
})
export class ClassTutorComponent implements OnInit {
courseId: any;
playlistId: any;
detail: ICourse;
playlist: [Object];
public result : any;
url: string = constants.IMAGE;
preview: any;
yurl: any = '';
model = {
  isactive: false,
  isNext: true
};
newarray = [
  {name: 'name', title:'New Array'},
  {name: 'name', title:'New Array'},
  {name: 'name', title:'New Array'},
  {name: 'name', title:'New Array'}

];
@BlockUI('videoblock') blockUI: NgBlockUI;
  constructor(private route: ActivatedRoute, private courseService: CourseService, private sanitizer: DomSanitizer) {
    this.route.params.forEach((params) => {
      this.courseId = params.id;
      this.playlistId = params.playlistId;
    });
   }

  ngOnInit() {
   this.getCourseDetail();
   this.getPlaylist();
  }

  getCourseDetail() {
    if (this.courseId) {
      this.courseService.getCourseDetail(this.courseId).subscribe(res => {
        this.detail = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
    }
  }

  getPlaylist() {
    this.courseService.youtubeService.getplaylist(this.playlistId).subscribe(res => {
      this.playlist = (<any>res).items;
      this.result = this.newarray.push.apply(this.playlist);
      console.log(this.result);
      this.preview = this.playlist.slice(0, 1);
     const videoId = this.preview[0].snippet.resourceId.videoId;
      this.playVideo(videoId);
    }, error => {
      console.log(error);
    });
  }

  playVideo(videoId, filtered?, isctive?) {
    this.blockUI.start('Loading....');
     this.model.isactive = true;
     // tslint:disable-next-line:max-line-length
     this.yurl =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoId + '?mode=opaque&amp;rel=0&amp;autohide=1&amp;showinfo=0&amp;wmode=transparent');
     this.blockUI.stop();
  }


}
