import { Component, OnInit } from '@angular/core';
import {ICourse} from "../../../shared/common/model/ICourse";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {CourseService} from "../../../admin/services/course.service";
import {BaseComponent} from "../../../shared/common/base/BaseComponent";

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent extends BaseComponent implements OnInit {
  coursecode: string;
  detail: ICourse;

  constructor(router: Router, private courseService: CourseService, private activatedRoute: ActivatedRoute) {
    super(null, router, courseService);
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.coursecode = params.get('id');

    });
    this.getdetail();
  }

  getdetail() {
    if (this.coursecode) {
      this.courseService.getCourseDetail(this.coursecode).subscribe(res => {
        this.detail = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
    }
  }




}
