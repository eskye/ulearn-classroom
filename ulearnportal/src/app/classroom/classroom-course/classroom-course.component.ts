import {AfterViewInit, Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {CourseService} from '../../admin/services/course.service';
import {ScriptLoaderService} from '../../shared/services/script-loader.service';
import {SystemConstant} from '../../shared/constant';
import {ICourse} from '../../shared/common/model/ICourse';
import {BaseComponent} from '../../shared/common/base/BaseComponent';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-classroom-course',
  templateUrl: './classroom-course.component.html',
  styleUrls: ['./classroom-course.component.css']
})
export class ClassroomCourseComponent extends BaseComponent implements OnInit, AfterViewInit {

  courses: ICourse[];
  playlist: [object];
  errorText: string;
  isError: boolean;
  showalert = false;
  url: string = SystemConstant.IMAGE;
  preview: any;
  yurl: any = '';
  constructor(private scriptLoader: ScriptLoaderService, private toastrService: ToastrService,
              private courseService: CourseService, private sanitizer: DomSanitizer) {
    super(null, null, courseService);
  }

  ngOnInit() {
    this.toastrService.error('hiii');
    this.listCourse();

  }

  ngAfterViewInit(): void {
    this.scriptLoader.loadScripts('app-admin-course', [
      'assets/js/plugins/forms/styling/uniform.min.js',
      'assets/js/demo_pages/form_inputs.js',
      'assets/js/demo_pages/learning.js'
    ]);
  }

  listCourse() {
    const pathid = 1;
    this.courseService.getCourse(pathid).subscribe(res => {
      this.courses = res;
    }, error => {
      console.log(error);
    });
  }

  getPlaylist(playlistId) {
    this.courseService.youtubeService.getplaylist(playlistId).subscribe(res => {
      this.playlist = (<any>res).items;
      // this.preview = (<any>this.playlist.items.slice(0, 1));
      this.preview = this.playlist.slice(0, 1);
      const videoId = this.preview[0].snippet.resourceId.videoId;
      this.yurl =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoId);

    }, error => {
      console.log(error);
    });
  }

  takeCourse(id) {
    const user = this.courseService.auth.Udata;
    this.courseService.enrolInCourse({student: user.id, course: id}).subscribe(res => {
      console.log(res);
    }, error => {
      this.errorText = error;
      this.isError = false;
      this.showalert = true;
      this.courseService.warningAlert(error, 'Warning');
    });

  }
}
