import { Component, OnInit } from '@angular/core';
import { ClassroomService } from '../services/classroom.service';
import { SystemConstant as constants } from 'src/app/shared/constant';

@Component({
  selector: 'app-classroom-dashboard',
  templateUrl: './classroom-dashboard.component.html',
  styleUrls: ['./classroom-dashboard.component.css']
})
export class ClassroomDashboardComponent implements OnInit {
items: any = [];
url: string = constants.IMAGE;
  constructor(private classroomService: ClassroomService) { }

  ngOnInit() {
    this.getCourseEnrolleedFor();
  }

  getCourseEnrolleedFor() {
    this.classroomService.getEnrollment().subscribe(res => {
       this.items = res;
    }, error => {
      console.log(error);
    });
  }

}
