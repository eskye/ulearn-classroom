import { Injectable, Injector } from '@angular/core';
import { IRootService, RootService, routes } from 'src/app/shared';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from 'src/app/shared/common/Error/ErrorHandler';
import { map } from 'rxjs/internal/operators/map';

export interface IClassRoomService extends IRootService<any> {
  getEnrollment(): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class ClassroomService extends RootService<any> implements IClassRoomService {

  constructor(httpClient: HttpClient, private injector: Injector, auth: AuthService) {
    super(httpClient, injector, auth);
  }

  getEnrollment(): Observable<any> {

    return this.post(routes.GETENROLLMENT, {student: this.auth.Udata.id}).pipe(map(res => {
       return res;
    }), catchError(ErrorHandler.handleError));
  }
}
