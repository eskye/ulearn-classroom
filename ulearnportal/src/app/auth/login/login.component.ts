import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../shared/services/script-loader.service';
import {IAccount, ITokenparams, IUserLogin} from '../../shared/common/model/IAccount';
import {routes} from '../../shared/constant';
import {SystemConstant as constants} from '../../shared/index';
import {AccountService} from '../service/account.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {BaseComponent} from '../../shared/common/base/BaseComponent';
import {DataSaverService} from '../../shared/services/data-saver.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit, AfterViewInit {
 auth: any = {};
  @BlockUI('login-ui') blockUI: NgBlockUI;
  showIcon: boolean;
  token: ITokenparams;
  errorText: string;
  isActive = false;
  constructor(   router: Router, private accountService: AccountService, private _script: ScriptLoaderService,
                 private dataService: DataSaverService, private titleService: Title) {
    super(null, router, accountService);
  }


  ngOnInit() {
    this.triggerGuard();
    this.titleService.setTitle('Login Page');
  }

 ngAfterViewInit()  {
  this._script.loadScripts('app-login',
      ['assets/js/demo_pages/login.js']);
  }

  signIn(f) {
    this.errorText = '';
    this.blockUI.start('We are signing you in...');
    this.showIcon = true;
    if (f.valid) {
      const login: IUserLogin = this.auth;
      return this.makeRequest(login);
      }
    this.showIcon = false;
    this.blockUI.stop();
    return this.errorText = 'Email and Password is required';
  }

  private makeRequest(login) {
    this.accountService.login(login)
      .subscribe((token) => {
      // console.log(token);
        this.KeepData(token);
        this.blockUI.stop();
        this.dataService.keepData('udata', token.user);
        if (token.role === constants.LEVEL_ACCESS.a) {
          this.router.navigate(['/admin/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.s) {
          this.router.navigate(['/classroom/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.m) {
            this.router.navigate(['/mentor/dashboard']);
        } else {
          this.router.navigate(['/']);
        }
      }, error => {
        this.showIcon = false;
        this.blockUI.stop();
        if (error.statusText.toLowerCase().includes('unknown error')) {
          this.errorText = 'Connection error, try again';
          this.blockUI.stop();
        } else {
          this.blockUI.stop();
          this.errorText = error.error.message;
        }
      });
  }

  triggerGuard() {
    if (!!this.dataService.getData('token') && !!this.dataService.getData('role')) {
      if (this.accountService.AuthService.role === constants.LEVEL_ACCESS.a) {
        this.router.navigate(['/admin/dashboard']);
      } else if (this.accountService.AuthService.role === constants.LEVEL_ACCESS.s) {
        this.router.navigate(['/classroom/dashboard']);
      } else if (this.accountService.AuthService.role === constants.LEVEL_ACCESS.m) {
       this.router.navigate(['/mentor/dashboard']);
      }   else {
        this.router.navigate(['/']);
      }
    }
  }

  private storeUserData(id) {

    return new Promise((resolve, reject) => {
      const reqHeader = {'Content-Type': 'application/json'};
      this.accountService.post(routes.LOGIN, {userId: id}, reqHeader)
        .subscribe(data => resolve(data.data), err => reject(err));
    });
  }

  protected KeepData(token) {
    this.dataService.keepData('token', token.token);
    this.dataService.keepData('role', token.role);
    // this.dataService.keepData('key', token.Key);
  }

}
