import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import {SharedModule} from '../shared/shared.module';
import {BlockUIModule} from 'ng-block-ui';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DataSaverService} from '../shared/services/data-saver.service';
import {CookieService} from 'ngx-cookie-service';
import { ActivationComponent } from './activation/activation.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot({
      message: 'Please Wait...',
    }),
    AuthRoutingModule,
    SharedModule
  ],
  declarations: [AuthComponent, LoginComponent, ActivationComponent],
  exports: [LoginComponent],

  providers: [
    DataSaverService,
    CookieService
  ]
})
export class AuthModule { }
