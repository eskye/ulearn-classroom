import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { ActivationComponent } from './activation/activation.component';

const routes: Routes = [
  {path: '', component: AuthComponent,
  children: [
    {path: '', component: LoginComponent},
    {path: 'activate/:key', component: ActivationComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
