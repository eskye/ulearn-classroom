import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { AccountService } from '../service/account.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {
  model: any = {};
  @BlockUI('activate-ui') blockUI: NgBlockUI;
  showIcon: boolean;
  errorText: string;
  isActive = false;
  constructor(private accountService: AccountService, private route: ActivatedRoute) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.model.key = params.get('key');
    });
   }

  ngOnInit() {
  }

  activate(f) {
    this.errorText = '';
    this.blockUI.start('We are signing you in...');
    this.showIcon = true;
    if (f.valid) {
      const key = this.model.key;
      this.accountService.activateUser(key).subscribe(res => {
        console.log(res);
        this.showIcon = false;
        this.blockUI.stop();
      }, error => {
        this.showIcon = false;
        this.blockUI.stop();
        this.errorText = error;
      });
      }
      this.blockUI.stop();
      this.errorText = 'Please enter activation code';
  }
}
