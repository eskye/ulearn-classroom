import { Injectable, Injector } from '@angular/core';

import { Observable } from 'rxjs/internal/observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


import {IRootService} from '../../shared/common/interface/IRootService';
import {IAccount, ITokenparams, IUserLogin} from '../../shared/common/model/IAccount';
import {RootService} from '../../shared/common/interface/RootService';
import {AuthService} from '../../shared/services/auth.service';
import {routes} from '../../shared/constant';
import {DataSaverService} from '../../shared/services/data-saver.service';
import { ErrorHandler } from 'src/app/shared/common/Error/ErrorHandler';

export interface IAccountService extends IRootService<IAccount> {
  login(logindata: IUserLogin): Observable<ITokenparams>;
  activateUser(key): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class AccountService extends RootService<IAccount>
  implements IAccountService {
  constructor(httpClient: HttpClient, private injector: Injector, auth: AuthService) {
    super(httpClient, injector, auth);
  }

  login(logindata: IUserLogin): Observable<ITokenparams> {

    const reqHeader = {
      'Content-Type': 'application/json',
      'No-Auth': 'True'
    };

    return this.post(routes.LOGIN, logindata, reqHeader).pipe(
      map(data => {
        return data.data as any;
      })
    );
  }
  activateUser(key): Observable<any> {
    return this.post(routes.ACTIVATEUSER, {key: key}).pipe(map(res => {
       return res;
    }), catchError(ErrorHandler.handleError));
  }
  //
  // sendPasswordMail(email: string) {
  //   return this.post(routes.FORGOTPASSWORD, { email: email }).pipe(
  //     map(res => {
  //       return res;
  //     })
  //   );
  // }

  // resetPassword(body: IResetPassword) {
  //   return this.post(routes.RESETPASSWORD, body).pipe(
  //     map(res => {
  //       return res;
  //     })
  //   );
  // }





  get AuthService(): AuthService {
    return this.getService(AuthService);
  }
  get store(): DataSaverService {
    return this.getService(DataSaverService);
  }
}
