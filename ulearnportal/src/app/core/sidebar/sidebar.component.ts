import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { EmitService } from 'src/app/shared/services/emit.service';
import { IProfile } from 'src/app/shared/common/model/IProfile';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  links: any;
  profile: IProfile;
  admin: any = [
    { linkname: 'Home', class: 'icon-home', link: '/admin/dashboard' },
    { linkname: 'Course', class: 'icon-file-text2', link: '/admin/course', sub: [
      {linkname: 'Add Course', class: 'fa fa-plus', link: 'admin/course/create'},
      {linkname: 'View Course',  class : 'fa fa-plus', link: 'admin/course/create'},
    ] },
    { linkname: 'Path', class: 'icon-stats-growth2', link: '/admin/path' },
    { linkname: 'Mentors', class: 'icon-users', link: '/admin/mentors' },
    { linkname: 'Users', class: 'icon-users', link: '/admin/users' },
    {linkname: 'Level', class: 'icon-stack', link: '/admin/level'}
  ];

  mentor: any = [
    { linkname: 'Home', class: 'icon-home', link: '/mentor/dashboard' },
    { linkname: 'MailBox', class: 'icon-envelop3', link: '/mentor/mail' },
    { linkname: 'Course', class: 'icon-file-text2', link: '/mentor/course', sub: [
      {linkname: 'Add Course', class: 'fa fa-plus', link: 'mentor/course/create'},
      {linkname: 'View Course',  class : 'fa fa-plus', link: 'mentor/course/create'},
    ] },
    { linkname: 'Profile', class: 'icon-user', link: '/mentor/message' }
  ];
  student: any = [
    { linkname: 'Home', class: 'icon-home', link: '/classroom/dashboard' },
    { linkname: 'Courses', class: 'icon-file-text2', link: '/classroom/courses'},

  ];
  constructor(private router: Router, private emitService: EmitService) {
    this.emitService.currentData$.subscribe(data => {
      this.profile = data;
    });
  }

  ngOnInit() {
    const url = this.router.url;
    // const url = <string>url.substring(url.lastIndexOf('/'),1).toString();
    // alert(url);

    if (url.startsWith('/admin') || url === '/admin') {
      this.links = this.admin;
    } else if (url.startsWith('/mentor') || url === '/mentor') {

      this.links = this.mentor;
    } else if (url.startsWith('/classroom') || url === '/classroom') {

      this.links = this.student;
    }
  }

}
