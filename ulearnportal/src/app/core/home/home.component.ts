import { Component, OnInit } from '@angular/core';
import { fadeAnimation } from '../../utils/animation';
import { EmitService } from 'src/app/shared/services/emit.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataSaverService } from 'src/app/shared/services/data-saver.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [fadeAnimation]
})
export class HomeComponent implements OnInit {
  active = true;
  data: any;
  loading = false;
  constructor(private emitservice: EmitService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private dataService: DataSaverService) {
      this.data = this.dataService.getData('udata');
      this.active = this.data.isActive;
      this.emitservice.changeData(this.data);
      this.emitservice.changeSideBarState(this.data.isActive);
      if (!this.data) {
        this.auth.getUserDetail().then(userdata => {
          this.data = userdata;
          this.active = this.data.isActive;
          this.emitservice.changeData(this.data);
          this.emitservice.changeSideBarState(this.data.isActive);
        }, err => {console.error(err); });
      }
     }

  ngOnInit() {
  }

}
