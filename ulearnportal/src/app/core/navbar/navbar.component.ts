import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/auth/service/account.service';
import { IProfile } from 'src/app/shared';
import { DataSaverService } from 'src/app/shared/services/data-saver.service';
declare var $:any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
profile: IProfile;
  constructor(private router: Router,
    private accountService: AccountService, private dataService: DataSaverService) {
     this.profile = this.accountService.auth.Udata;
     console.log(this.profile);
     }

  ngOnInit() {}

  toggleNav() {
    const body$ = $('body');
    if (body$.hasClass('sidebar-xs') || !body$.hasClass('sidebar-xs')) {
       body$.toggleClass('sidebar-mobile-main');
    }
    $('.sidebar-main-toggle').click(function (e) {
      // e.preventDefault();
      $('body').toggleClass('sidebar-xs').removeClass('sidebar-mobile-main');
  });
  }

  logout() {
    const allCookies: {} = this.dataService.getAllCookie();
   if (allCookies) {
      this.dataService.cookService.deleteAll('/');
      this.router.navigate(['/']);

   }
   this.dataService.removePersitedData('token');
   this.dataService.removePersitedData('role');
   this.dataService.removePersitedData('udata');
    this.router.navigate(['/']);
  }
}
