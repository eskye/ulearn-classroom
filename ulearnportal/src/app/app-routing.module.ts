import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './core/home/home.component';
import {RoleGuardService} from './shared/services/route-guards/role-guard.service';

const routes: Routes = [
  { path: '', loadChildren: './auth/auth.module#AuthModule' },
  {path: 'dashboard', component: HomeComponent},
  {
    path: 'admin',
    component: HomeComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'Admin'
    },
    children: [
      { path: '',
       loadChildren: './admin/admin.module#AdminModule',
     }
    ]
  },

  {
    path: 'classroom',
    component: HomeComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'Student'
    },
    children: [
      { path: '',
        loadChildren: './classroom/classroom.module#ClassroomModule',
      }
    ]
  },
  {
    path: 'mentor',
    component: HomeComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'Mentor'
    },
    children: [
      {
        path: '',
        loadChildren: './mentor/mentor.module#MentorModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
