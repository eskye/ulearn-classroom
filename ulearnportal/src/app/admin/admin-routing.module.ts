import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { AdminPathComponent } from './admin-path/admin-path.component';
import { AdminCourseComponent } from './admin-course/admin-course.component';
import { AdminLevelComponent } from './admin-level/admin-level.component';
import {CourseDetailsComponent} from './admin-course/course-details/course-details.component';
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import { MentorManagementComponent } from './mentor-management/mentor-management.component';
import { AssignMentorComponent } from './assign-mentor/assign-mentor.component';

const routes: Routes = [
  {
    path: '', component: AdminDashboardComponent
  },
  {
    path: 'dashboard', component: AdminDashboardComponent
  },
  {
    path: 'users', component: UsersComponent
  },
  {
    path: 'mentors', component: MentorManagementComponent
  },
  {
    path: 'path', component: AdminPathComponent
  },
  {
    path: 'level', component: AdminLevelComponent
  },
  {
    path: 'assignmentor', component: AssignMentorComponent
  },
  {path: 'course', component: AdminCourseComponent},
  {path: 'course/:id/details', component: CourseDetailsComponent},
  {path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
