import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared';
import { IAdminService, AdminService } from '../services/admin.service';

@Component({
  selector: 'app-assign-mentor',
  templateUrl: './assign-mentor.component.html',
  styleUrls: ['./assign-mentor.component.css']
})
export class AssignMentorComponent extends BaseComponent implements OnInit {
  mentors: any;

  constructor(private adminService: AdminService) {
    super(null, null, adminService);
  }

  ngOnInit() {
    this.getMentors();
  }


  getMentors() {
    this.adminService.getMentors().subscribe(res =>{
      this.mentors = res.mentors.data;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      console.log(error);
    });
  }
}
