import { Injectable, Injector } from '@angular/core';
import { IRootService, RootService } from 'src/app/shared';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdminService } from './admin.service';
import {Observable} from 'rxjs/index';
import {routes} from '../../shared/constant';
import {catchError, map} from 'rxjs/internal/operators';
import {ErrorHandler} from '../../shared/common/Error/ErrorHandler';
import {ICourse} from '../../shared/common/model/ICourse';
import { YoutubeService } from 'src/app/shared/services/youtube.service';
import {AuthService} from '../../shared/services/auth.service';
import {IProfile} from '../../shared/common/model/IProfile';

export interface ICourseService extends IRootService<any> {
 createCourse(data: FormData): Observable<any>;
  getCourse(pathid): Observable<ICourse[]>;
  getUserInCourse(body): Observable<IProfile[]>;
  getCourseDetail(coursecode): Observable<ICourse>;
  enrolInCourse(body): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class CourseService extends RootService<any> implements ICourseService {

  constructor(httpClient: HttpClient, private injector: Injector, auth: AuthService ) {
    super(httpClient, injector, auth);
  }

  get adminService(): AdminService {
    return this.injector.get(AdminService);
  }
  get youtubeService(): YoutubeService {
    return this.injector.get(YoutubeService);
  }

  createCourse(data: FormData): Observable<any> {
    const reqHeader = new HttpHeaders(
      {'Authorization': `Bearer ${this.auth.Token}` });
    return this.httpClient.post(routes.CREATECOURSE, data, {headers: reqHeader}).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }

  getCourse(pathid): Observable<ICourse[]> {
    return this.get(pathid, routes.LISTCOURSE).pipe(map(res => {
      return res.data;
    }), catchError(ErrorHandler.handleError));
  }

  getCourseDetail(coursecode): Observable<ICourse> {
    return this.get(coursecode, routes.COURSEDETAILS).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }
  getUserInCourse(body): Observable<IProfile[]> {
    return this.post(routes.GETUSERINCOURSE, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }

  enrolInCourse(body): Observable<any> {
    return this.post(routes.ENROL, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }
}
