import { Injectable, Injector } from '@angular/core';
import { IRootService, RootService, routes } from '../../shared';
import { Observable } from 'rxjs/internal/observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandler } from '../../shared/common/Error/ErrorHandler';
import { IPath, ILevel } from '../../shared/common/model/IPath';
import {AuthService} from '../../shared/services/auth.service';

export interface IAdminService extends IRootService<any> {
  getUsers(role): Observable<any>;
  createPath(data: IPath): Observable<any>;
  listPath(): Observable<IPath[]>;
  createLevel(data: ILevel): Observable<any>;
  listLevel(): Observable<ILevel[]>;
  getMentors(): Observable<any>;
  getRoles(): Observable<any>;
  activateAllAccounts(data: any): Observable<any>;

}

@Injectable({
  providedIn: 'root'
})
export class AdminService extends RootService<any> implements IAdminService {


  constructor(httpClient: HttpClient, private injector: Injector, auth: AuthService) {
    super(httpClient, injector, auth);
  }

  getUsers(role): Observable<any> {
   return this.getlist(routes.GETUSERS + '/' + `${role}`).pipe(map(res => {
     return res;
   }), catchError(ErrorHandler.handleError));
  }

  createPath(data: IPath): Observable<any> {
    return this.post(routes.CREATEPATH, data).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }

  listPath(): Observable<IPath[]> {
    return this.getlist(routes.LISTPATH).pipe(map(res => {
      return res.path;
    }), catchError(ErrorHandler.handleError));
   }

  createLevel(data: ILevel): Observable<any> {
    return this.post(routes.CREATELEVEL, data).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
  }

  listLevel(): Observable<ILevel[]> {
    return this.getlist(routes.LISTLEVEL).pipe(map(res => {
      return res.level;
    }), catchError(ErrorHandler.handleError));
   }

   getMentors(): Observable<any> {
    return this.getlist(routes.GETMENTORS).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
   }
   getRoles(): Observable<any> {
    return this.getlist(routes.ROLES).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.handleError));
   }
   activateAllAccounts(data: any): Observable<any>{
     return this.post(routes.ACTIVATEUSERS, data).pipe(map(res =>{
       return res;
     }), catchError(ErrorHandler.handleError));
   }
}
