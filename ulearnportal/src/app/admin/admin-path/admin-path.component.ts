import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../shared/services/script-loader.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IPath } from '../../shared/common/model/IPath';
import { BaseComponent } from '../../shared';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-path',
  templateUrl: './admin-path.component.html',
  styleUrls: ['./admin-path.component.css']
})
export class AdminPathComponent extends BaseComponent implements OnInit, AfterViewInit {
  pathform: FormGroup;
  isloggingin: boolean;
  errorText: any;
  paths: IPath[];
  constructor(router: Router, private adminService: AdminService, private scriptLoader: ScriptLoaderService) {
    super(null, router);
  }

  ngOnInit() {
    this.validateField();
    this.listPath();
  }

  ngAfterViewInit() {
    this.scriptLoader.loadScripts('app-admin-path', [
     // 'assets/js/demo_pages/components_modals.js',
      'assets/js/plugins/forms/selects/select2.min.js'
    ]);
  }
  validateField() {
    this.pathform = new FormGroup({
          pathname: new FormControl('', [Validators.required]),
          description: new FormControl('')
        });
  }

  createPath() {
    this.errorText = '';
    if (this.pathform.valid) {
      this.isloggingin = true;
       const pathData: IPath = this.pathform.value;
        this.pathform.setErrors({invalid: true});
       this.adminService.createPath(pathData).subscribe(res => {
        this.isloggingin = false;
        this.paths.push(res);
        this.pathform.reset();
       }, error => {
        this.errorText = error;
         this.isloggingin = false;

       });
    }
  }

  listPath() {
    this.waiting = true;
    this.adminService.listPath().subscribe(res => {
      this.paths = res;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      console.log(error);
    });
  }

}
