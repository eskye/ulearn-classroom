import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import { ScriptLoaderService } from 'src/app/shared/services/script-loader.service';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { BaseComponent } from 'src/app/shared';
import { CourseService } from '../services/course.service';
import { IPath, ILevel } from 'src/app/shared/common/model/IPath';
import {ICourse} from '../../shared/common/model/ICourse';
import {SystemConstant} from '../../shared/constant';
import { Playlist } from 'src/app/shared/common/model/Playlist';
import {DomSanitizer} from '@angular/platform-browser';
declare var $: any;


@Component({
  selector: 'app-admin-course',
  templateUrl: './admin-course.component.html',
  styleUrls: ['./admin-course.component.css']
})
export class AdminCourseComponent extends BaseComponent implements OnInit, AfterViewInit {

 courseform: FormGroup;
paths: IPath[];
levels: ILevel[];
courses: ICourse[];
playlist: [object];
errorText: string;
url: string = SystemConstant.IMAGE;
@ViewChild('courseposter') courseposter: ElementRef;
  preview: any;
  yurl: any = '';

  constructor(private fb: FormBuilder, private scriptLoader: ScriptLoaderService,
     private courseService: CourseService, private sanitizer: DomSanitizer) {
    super();
  }

  ngOnInit() {
    this.listCourse();
    this.validate();
  }

  ngAfterViewInit(): void {
    this.scriptLoader.loadScripts('app-admin-course', [
      'assets/js/plugins/forms/styling/uniform.min.js',
      'assets/js/demo_pages/form_inputs.js',
      'assets/js/demo_pages/learning.js'
    ]);
  }

  validate() {
    this.courseform = this.fb.group({
       coursename: ['', Validators.required],
       description: ['', Validators.required],
       path: ['', Validators.required],
       level: ['', Validators.required],
       resource: ['', Validators.required],
      coursethumbnail: null

    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
        this.courseform.get('coursethumbnail').setValue(file);

    }
  }
loadFormData() {
  this.listPath();
  this.listLevel();
}

closeCloneModal() {
  const originalModal = $('#course_preview').clone();
$(document).on('#course_preview', 'hidden.bs.modal', function () {
    $('#course_preview').remove();
    const myClone = originalModal.clone();
    $('body').append(myClone);
});
}
closeModal() {
  $('.modal').on('hidden.bs.modal', function() {
    $('.modal-body').html('');
});
}
  clearFile() {
    this.courseform.get('coursethumbnail').setValue(null);
    this.courseposter.nativeElement.value = '';
  }

  private prepareSave(): FormData {
    const inputs = new FormData();
    inputs.append('coursename', this.courseform.get('coursename').value);
    inputs.append('description', this.courseform.get('description').value);
    inputs.append('path', this.courseform.get('path').value);
    inputs.append('level', this.courseform.get('level').value);
    inputs.append('banner', this.courseform.get('coursethumbnail').value);
    inputs.append('resource', this.courseform.get('resource').value);
    return inputs;
  }

  createCourse() {
    const formModel = this.prepareSave();
    this.courseService.createCourse(formModel).subscribe(res => {
      this.clearFile();
      $('#modalcourse').close();
      console.log(res);
    }, error => {
      this.clearFile();
      this.errorText = error;
    });
  }

  listCourse() {
    const pathid = 1;
    this.courseService.getCourse(pathid).subscribe(res => {
      console.log(res);
      this.courses = res;
    }, error => {
      console.log(error);
    });
  }


  listPath() {
    this.courseService.adminService.listPath().subscribe(res => {
      console.log(res);
      this.paths = res;
    }, error => {
      console.log(error);
    });
  }

   listLevel() {
    this.courseService.adminService.listLevel().subscribe(res => {
      this.levels = res;
    }, error => {
      console.log(error);
    });
  }

  getPlaylist(playlistId) {
    this.courseService.youtubeService.getplaylist(playlistId).subscribe(res => {
      this.playlist = (<any>res).items;
        // this.preview = (<any>this.playlist.items.slice(0, 1));
     this.preview = this.playlist.slice(0, 1);
     const videoId = this.preview[0].snippet.resourceId.videoId;
     this.yurl =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoId);

    }, error => {
      console.log(error);
    });
  }

}
