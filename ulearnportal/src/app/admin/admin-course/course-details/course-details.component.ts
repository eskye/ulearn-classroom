import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../../../shared/common/base/BaseComponent';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {CourseService} from '../../services/course.service';
import {ICourse} from '../../../shared/common/model/ICourse';
import {IProfile} from '../../../shared/common/model/IProfile';


@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent extends BaseComponent implements OnInit {
 coursecode: string;
 detail: ICourse;
  users: IProfile[];
  constructor(router: Router, private courseService: CourseService, private activatedRoute: ActivatedRoute) {
    super(null, router, courseService);
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.coursecode = params.get('id');

    });
    this.getdetail();
  }

  getdetail() {
    if (this.coursecode) {
      this.courseService.getCourseDetail(this.coursecode).subscribe(res => {
        this.detail = res;
      }, error => {
        this.courseService.errorAlert(error, 'Error');
      });
    }
  }

  getUserInCourse(id) {
    this.courseService.getUserInCourse({course: id}).subscribe(res => {
      this.users = res;
    }, error => {
      console.log(error);
    });
  }

}
