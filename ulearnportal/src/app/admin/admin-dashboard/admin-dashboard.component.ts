import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/base/BaseComponent';
import { IDashboardService, DashboardService } from 'src/app/shared/services/dashboard.service';
import { IDashboard } from 'src/app/shared/common/model/IDashboard';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent extends BaseComponent implements OnInit {
counter: IDashboard;
  constructor(private dashboardService: DashboardService) {
    super(null, null, dashboardService);
  }

  ngOnInit() {
    this.dashboardService.getAdminDashboard().subscribe(res => {
      this.counter = res;
    }, error => {
      console.log(error);
    });
  }

}
