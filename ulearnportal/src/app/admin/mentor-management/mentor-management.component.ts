import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/base/BaseComponent';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-mentor-management',
  templateUrl: './mentor-management.component.html',
  styleUrls: ['./mentor-management.component.css']
})
export class MentorManagementComponent extends BaseComponent implements OnInit {
  mentors = [];
  constructor(private adminService: AdminService) {
    super(null, null, adminService);
  }

  ngOnInit() {
    this.waiting = true;
    this.adminService.getMentors().subscribe(res =>{
      this.mentors = res.mentors.data;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      console.log(error);
    });
  }

}
