import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../shared';
import { AdminService } from '../services/admin.service';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent extends BaseComponent implements OnInit {
users: any = [];
roles: Array<any> = [];
role: any = 0;
  selected: any;
  selects: Array<any> = [];
  constructor(private adminService: AdminService) {
    super(null, null, adminService);
  }

  ngOnInit() {
    this.getRoles();
    this.getUsers();
  }

  getRoles() {
    this.adminService.getRoles().subscribe(res => {
      const roles: Array<any> = res.roles;
       roles.filter((f) => {
          if (f.rolename === 'Admin') {
              roles.splice(f[0], 1);
          }
       });
      this.roles = roles;

    });
  }

  getUsers() {
    this.waiting = true;
    this.adminService.getUsers(this.role).subscribe(res => {
      this.users = res.users.data;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      console.log(error);
    });
  }

  activateAccounts() {
    if (this.role === 0) {
        this.adminService.warningAlert('Please kindly select the type of account role you want to activate');
        return false;
    }
    const body = {role: this.role};
    this.adminService.activateAllAccounts(body).subscribe(res => {
      console.log(res);
    });
  }

  toggleCheck(e) {
   this.selected = e.target.checked;
  }

  checkAll(e) {
    this.users.forEach(element => {
       this.selected = e.target.checked;
       if (this.selected) {
        this.selects.push(element);
       }
    });
    return false;
  }


  private sendRequest() {
    if (this.selects.length > 0) {
  }
}

}
