import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../shared/services/script-loader.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILevel } from '../../shared/common/model/IPath';
import { BaseComponent } from '../../shared';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-level',
  templateUrl: './admin-level.component.html',
  styleUrls: ['./admin-level.component.css']
})
export class AdminLevelComponent extends BaseComponent implements OnInit, AfterViewInit {
  levelform: FormGroup;
  isloggingin: boolean;
  errorText: any;
  levels: ILevel[];
  constructor(router: Router, private adminService: AdminService, private scriptLoader: ScriptLoaderService) {
    super(null, router);
  }

  ngOnInit() {
    this.validateField();
    this.listLevel();
  }

  ngAfterViewInit() {
    this.scriptLoader.loadScripts('app-admin-level', [
     // 'assets/js/demo_pages/components_modals.js',
      'assets/js/plugins/forms/selects/select2.min.js'
    ]);
  }
  validateField() {
    this.levelform = new FormGroup({
          levelname: new FormControl('', [Validators.required]),
        });
  }

  createLevel() {
    this.errorText = '';
    if (this.levelform.valid) {
      this.isloggingin = true;
       const levelData: ILevel = this.levelform.value;
        this.levelform.setErrors({invalid: true});
        console.log(levelData);
       this.adminService.createLevel(levelData).subscribe(res => {
        this.isloggingin = false;
        this.levels.push(res);
        this.levelform.reset();
       }, error => {
        this.errorText = error;
         this.isloggingin = false;

       });
    }
  }

  listLevel() {
    this.adminService.listLevel().subscribe(res => {
      console.log(res);
      this.levels = res;
    }, error => {
      console.log(error);
    });
  }

}
