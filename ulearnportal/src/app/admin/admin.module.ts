import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { UsersComponent } from './users/users.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminCourseComponent } from './admin-course/admin-course.component';
import { AdminPathComponent } from './admin-path/admin-path.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLevelComponent } from './admin-level/admin-level.component';
import { AssignMentorComponent } from './assign-mentor/assign-mentor.component';
import { CourseDetailsComponent } from './admin-course/course-details/course-details.component';
import { MentorManagementComponent } from './mentor-management/mentor-management.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    AdminRoutingModule
  ],
  declarations: [UsersComponent,
     AdminDashboardComponent,
      AdminCourseComponent,
      AdminPathComponent,
      AdminLevelComponent,
      AssignMentorComponent,
      CourseDetailsComponent,
      MentorManagementComponent
    ]
})
export class AdminModule { }
